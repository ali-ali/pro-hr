import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzModalService, NzMessageService, NzTableSortOrder, NzTableSortFn, NzTableFilterList, NzTableFilterFn, NzTableComponent } from 'ng-zorro-antd';
import { Subject } from 'rxjs';

import { takeUntil } from 'rxjs/operators';
export class ItemData {
  index: number;
  id: number;
  name: string;
  age: number;
  address: string;
  disabled: boolean;
}
class CustomColumn {
  sortOrder?: NzTableSortOrder;
  sortFn?: NzTableSortFn;
  sortDirections?: NzTableSortOrder[];
}
interface ColumnItem {
  name: string;
  sortOrder?: NzTableSortOrder;
  sortFn?: NzTableSortFn;
  listOfFilter?: NzTableFilterList;
  filterFn?: NzTableFilterFn;
  filterMultiple?: boolean;
  sortDirections?: NzTableSortOrder[];
}
@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.less']
})
export class EmployeeComponent implements OnInit , AfterViewInit, OnDestroy{  
  @ViewChild('table', { static: false }) nzTableComponent?: 
  NzTableComponent<ItemData>;
  pageIndex = 1;
  
  private destroy$ = new Subject();
  pageSize = 10;
  searchValue = '';
  checked = false;
  loading = false;
  name_col : CustomColumn;
  indeterminate = false;
  listOfCurrentPageData: ItemData[] = [];
  setOfCheckedId = new Set<number>();
  isVisible: boolean;
  editStatus: boolean;
  input : ItemData = new ItemData();
  validateForm: FormGroup;
  listOfData: ItemData[] = [];
  listOfDisplayData: ItemData[] = [];
  listOfColumns: ColumnItem[] = [
    {
      name: 'age',
      sortOrder: null,
      sortFn: (a: ItemData, b: ItemData) => a.age - b.age
    },
    {
      name: 'address',
      sortOrder: null,
      sortFn: (a: ItemData, b: ItemData) => a.address.localeCompare(b.address),
      filterMultiple: false,
      listOfFilter: [
        { text: 'London', value: 'London' },
        { text: 'Sidney', value: 'Sidney' },
        { text: 'Damascus', value: 'Damascus' },
        { text: 'Homs', value: 'Homs' }
      ],
      filterFn: (address: string, item: ItemData) => item.address.indexOf(address) !== -1
    }
  ];
  visible: boolean;
  constructor(private _router: Router,private fb: FormBuilder, 
    private modalService: NzModalService){

    }
    scrollToIndex(index: number): void {
      debugger;
      this.nzTableComponent?.cdkVirtualScrollViewport?.scrollToIndex(index);
    }
    trackByIndex(_: number, data: ItemData): number {
      return data.index;
    }
    reset(): void {
      this.searchValue = '';
      this.search();
    }
    ngAfterViewInit(): void {
      this.nzTableComponent?.cdkVirtualScrollViewport?.scrolledIndexChange.pipe(takeUntil(this.destroy$)).subscribe((data: number) => {
        console.log('scroll index to', data);
      });
    }
    ngOnDestroy(): void {
      this.destroy$.next();
      this.destroy$.complete();
    }
    search(): void {
      this.visible = false;
      this.listOfDisplayData = this.listOfData.filter((item: ItemData) => item.name.indexOf(this.searchValue) !== -1);
    }
  ngOnInit(): void {
    debugger;
    for (let i = 0; i < 35; i++) {
      this.listOfData.push({
        index: i,
        id: i,
        name:  `Jim`,
        age: i/2 == 2 ? 32 : 18 + i,
        address: i/2 == 2 ? `London` :  `Sidney`,
        disabled : false
      });
    }
    this.listOfData.forEach(element => {
      if(element.id < 10){
        element.name = `Joy`;
        element.address = `Homs`;
      }
      if(element.id > 10 && element.id < 16){
        element.name = `Jim`;
        element.address = `Damascus`;
      }
      if(element.id > 16 && element.id < 20){
        element.name = `Joi`;
        element.address = `London`;
      }
    });
    this.name_col = new CustomColumn();
    this.name_col.sortOrder = null;
    this.name_col.sortFn = (a: ItemData, b: ItemData) => a.name.localeCompare(b.name);
    this.listOfDisplayData = [...this.listOfData];
    this.validateForm = this.fb.group({
      name : [null, [Validators.required]],
      age : [null, [Validators.required,Validators.min(18)]],
      address : [null, [Validators.required]],
      
    });
  }
  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onCurrentPageDataChange(listOfCurrentPageData: ItemData[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }
  onChangePageSize(size : number): void{
    debugger;
    this.pageSize = size;
    this.scrollToIndex(0);
  }
  onChangePageIndex(index : number): void{
    this.pageIndex = index;
    this.scrollToIndex(0);
    debugger;
  }
  updateIndexes(e)
{
  debugger;
}  refreshCheckedStatus(): void {
    const listOfEnabledData = this.listOfCurrentPageData.filter(({ disabled }) => !disabled);
    this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(checked: boolean): void {
    this.listOfCurrentPageData.filter(({ disabled }) => !disabled).forEach(({ id }) => this.updateCheckedSet(id, checked));
    this.refreshCheckedStatus();
  }
  sendRequest(): void {
    this.loading = true;
    const requestData = this.listOfData.filter(data => this.setOfCheckedId.has(data.id));
    console.log(requestData);
    setTimeout(() => {
      this.setOfCheckedId.clear();
      this.refreshCheckedStatus();
      this.loading = false;
    }, 1000);
  }
  showModal(): void {
    this.isVisible = true;
  }
create(){
  this.editStatus = false;
  this.showModal();
}
handleOk(): void {
  console.log(this.validateForm.value);
          var valid = true;
          for (const i in this.validateForm.controls) {
            this.validateForm.controls[i].markAsDirty();
            this.validateForm.controls[i].updateValueAndValidity();
            if(!this.validateForm.controls[i].valid)
               valid = false;
          }
}

handleCancel(): void {
  console.log('Button cancel clicked!');
  this.isVisible = false;
}
}
