import { NgModule } from '@angular/core';

import { EmployeeRoutingModule } from './employee-routing.module';

import { EmployeeComponent} from './employee.component';
import { NzTableModule } from 'ng-zorro-antd/table';

import { NzDividerModule } from 'ng-zorro-antd/divider';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { AddEmployeeComponent } from './add-employee/add-employee.component';

import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NzButtonModule, NzSelectModule, 
  NzDropDownModule, NzRadioModule, NzDatePickerModule, NzCheckboxModule,
  NzInputModule, NzCardModule,NzAutocompleteModule,NzFormModule , NzModalModule, NzTreeModule, NzAffixModule, NzTabsModule, NzMessageService} from 'ng-zorro-antd';
import { RouterModule } from '@angular/router';
@NgModule({
  imports: [
    NzTableModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    NzIconModule,
    RouterModule,
    NzCardModule,
    NzButtonModule,
    NzSelectModule,
    NzTabsModule,
    NzDividerModule,
    EmployeeRoutingModule,
    NzDropDownModule,
    NzAutocompleteModule,
    NzRadioModule,
    NzModalModule,
    NzDatePickerModule,
    NzTreeModule,
    NzRadioModule,
    NzAffixModule,
NzCheckboxModule,
NzInputModule ,
NzFormModule
  ],
  declarations: [EmployeeComponent, AddEmployeeComponent],
  exports: [EmployeeComponent]
})
export class EmployeeModule { }
