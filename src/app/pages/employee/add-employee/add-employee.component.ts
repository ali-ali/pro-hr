import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls:['./add-employee.component.less']
})
export class AddEmployeeComponent implements OnInit , AfterViewInit, OnDestroy{
  
  constructor() {
  }
    ngOnDestroy(): void {
    }
    ngAfterViewInit(): void {
    }
    ngOnInit(): void {
    }  
    
}
